from trytond.report import Report
from trytond.pool import Pool


class PrintAfiliationState(Report):
    "Print Incluir Salud Medication - Report"
    __name__ = 'gnuhealth.incluir_salud.print_afiliation_state.report'

    @classmethod
    def get_context(cls, records, header, data):
        context = super().get_context(records, header, data)
        pool = Pool()
        Afiliation = pool.get('gnuhealth.incluir_salud.afiliation')
        afiliations = Afiliation.search([
            ('month', '=', data['month']),
            ('year', '=', data['year']),
            ('state_afiliation', 'in', data['states'])
            ])
        context['afiliations'] = afiliations

        return context
