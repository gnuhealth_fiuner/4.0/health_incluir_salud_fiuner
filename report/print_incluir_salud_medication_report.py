from trytond.report import Report
from trytond.pool import Pool


class PrintIncluirSaludMedicationReport(Report):
    "Print Incluir Salud Medication - Report"
    __name__ = 'gnuhealth.incluir_salud.print_medication.report'


    @classmethod
    def get_context(cls, records, header, data):
        context = super().get_context(records, header, data)
        pool = Pool()
        Medication = pool.get('gnuhealth.incluir_salud.medication')
        medications = Medication.search([
            ('afiliation.month', '=', data['month']),
            ('afiliation.year', '=', data['year']),
            ('afiliation.state_afiliation', 'in', data['states'])
            ])
        context['medications'] = medications

        # buscamos las dosis correspondientes a cada medicacion (medication, strength, unit)
        med_str_unit = list(set([(m.medication) for m in medications]))

        # recorremos el listado de medicación y vamos conformando los totales
        context['formulas'] = {}
        for formula in med_str_unit:
            total = sum([m.monthly for m in medications
                         if (m.medication) == formula])
            context['formulas'][formula] = {
                'name': ' '.join([
                        formula.rec_name,
                        ]),
                'total': total
                }

        return context
