import csv
import sys


def main(args):
    head = '\n'.join([
        '<?xml version="1.0"?>',
        '<tryton>',
        4*' ' + '<data skiptest="1" noupdate="1">'
        ])
    foot =  '\n'.join([
        4*' ' + '</data>',
        '</tryton>'
        ])
    line = 0
    templates = []
    products = []
    medicines = []
    with open('vademecum.csv', 'r') as csvfile:
        read_csv = csv.reader(csvfile, delimiter=',', quotechar='"')
        next(read_csv)
        for row in read_csv:
            line +=1
            name = row[0]
            strength = row[1].strip().replace(',', '.')
            unit = row[2].lower().strip()
            is_solution = row[3]
            sol_conc = row[4].strip().replace(',', '.')
            sol_conc_unit = row[5]
            sol_volume = row[6].strip().replace(',', '.')
            sol_volume_unit = row[7]
            presentation = row[8]
            snomed_id = row[9]
            desc = row[10]
            _id = str(line).zfill(3)

            unit = \
                'health.mg' if unit == 'mg' \
                else 'health.ug' if unit == 'mcg' \
                else 'health_incluir_salud_fiuner.dose_unit_01' if unit == 'mg/1ml' \
                else 'health_incluir_salud_fiuner.dose_unit_02' if unit == 'mg/2ml' \
                else 'health_incluir_salud_fiuner.dose_unit_03' if unit == 'mg/3ml' \
                else 'health_incluir_salud_fiuner.dose_unit_04' if unit == 'mg/4ml' \
                else 'health_incluir_salud_fiuner.dose_unit_05' if unit == 'mcg x 200d' \
                else 'health_incluir_salud_fiuner.dose_unit_06' if unit == '% 5ml' \
                else 'health_incluir_salud_fiuner.dose_unit_08' if unit == 'g' \
                else 'health_incluir_salud_fiuner.dose_unit_07' if unit == 'mg/ml' \
                else 'health_incluir_salud_fiuner.dose_unit_09' if unit == '%' \
                else 'health.unit'

            sol_conc_unit = \
                'health.ml' if sol_conc_unit == 'ml' \
                else 'health.ug' if sol_conc_unit == 'mcg' \
                else 'health.mg' if sol_conc_unit == 'mg' \
                else 'health_incluir_salud_fiuner.dose_unit_01' if sol_conc_unit == 'mg/1ml' \
                else 'health_incluir_salud_fiuner.dose_unit_02' if sol_conc_unit == 'mg/2ml' \
                else 'health_incluir_salud_fiuner.dose_unit_03' if sol_conc_unit == 'mg/3ml' \
                else 'health_incluir_salud_fiuner.dose_unit_04' if sol_conc_unit == 'mg/4ml' \
                else 'health_incluir_salud_fiuner.dose_unit_05' if sol_conc_unit == 'mcg x 200d' \
                else 'health_incluir_salud_fiuner.dose_unit_06' if sol_conc_unit == '% 5ml' \
                else 'health_incluir_salud_fiuner.dose_unit_08' if sol_conc_unit == 'g' \
                else 'health_incluir_salud_fiuner.dose_unit_07' if sol_conc_unit == 'mg/ml' \
                else 'health_incluir_salud_fiuner.dose_unit_09' if sol_conc_unit == '%' \
                else None

            sol_volume_unit =\
                'health.ml' if sol_volume_unit == 'ml' \
                else 'health_incluir_salud_fiuner.dose_unit_01' if sol_volume_unit == 'mg/1ml' \
                else 'health_incluir_salud_fiuner.dose_unit_02' if sol_volume_unit == 'mg/2ml' \
                else 'health_incluir_salud_fiuner.dose_unit_03' if sol_volume_unit == 'mg/3ml' \
                else 'health_incluir_salud_fiuner.dose_unit_04' if sol_volume_unit == 'mg/4ml' \
                else 'health_incluir_salud_fiuner.dose_unit_05' if sol_volume_unit == 'disparos' \
                else 'health_incluir_salud_fiuner.dose_unit_06' if sol_volume_unit == '% 5ml' \
                else 'health_incluir_salud_fiuner.dose_unit_08' if sol_volume_unit == 'g' \
                else 'health_incluir_salud_fiuner.dose_unit_07' if sol_volume_unit == 'mg/ml' \
                else 'health_incluir_salud_fiuner.dose_unit_09' if sol_volume_unit == '%' \
                else None

            template = '\n'.join([
                8*' ' + f'<record model="product.template" id="template_{_id}">',
                12*' ' + f'<field name="name">{name}</field>',
                12*' ' + '<field name="default_uom" model="product.uom" ref="product.uom_unit"/>',
                12*' ' + '<field name="list_price" eval="0"/>',
                8*' ' + '</record>'
                ])
            product = '\n'.join([
                8*' ' + f'<record model="product.product" id="product_{_id}">',
                12*' ' + f'<field name="template" model="product.template" ref="template_{_id}"/>',
                12*' ' + '<field name="is_medicament">1</field>',
                12*' ' + '<field name="gtin_type">custom</field>',
                8*' ' + '</record>'
                ])
            medicine_list = []
            medicine1 = '\n'.join([
                8*' ' + f'<record model="gnuhealth.medicament" id="meds_{_id}">',
                12*' ' + f'<field name="name" model="product.product" ref="product_{_id}"/>',
                12*' ' + f'<field name="active_component">{name}</field>',
                12*' ' + f'<field name="notes">{desc}</field>',
                12*' ' + f'<field name="snomed_id">{snomed_id}</field>',
                ])
            medicine_list.append(medicine1)
            if strength.strip() != '':
                medicine2 = '\n'.join([
                    12*' ' + f'<field name="strength">{strength}</field>',
                    12*' ' + f'<field name="unit" model="gnuhealth.dose.unit" ref="{unit}"/>',
                    ])
                medicine_list.append(medicine2)
            if is_solution.lower().strip() == "si":
                medicine3 = '\n'.join([
                    12*' ' + f'<field name="sol_conc">{sol_conc}</field>',
                    12*' ' + f'<field name="sol_conc_unit" model="gnuhealth.dose.unit" '
                            f'ref="{sol_conc_unit}"/>',
                    12*' ' + f'<field name="sol_vol">{sol_volume}</field>',
                    12*' ' + f'<field name="sol_vol_unit" model="gnuhealth.dose.unit" '
                            f'ref="{sol_volume_unit}"/>',
                    ])
                medicine_list.append(medicine3)
            medicine4 = 8*' ' + '</record>'
            medicine_list.append(medicine4)
            medicine = '\n'.join(medicine_list)

            templates.append(template)
            products.append(product)
            medicines.append(medicine)

    with open('templates.xml', 'w') as file_:
        templates = '\n\n'.join(templates)
        output = '\n'.join([
            head,
            templates,
            foot
            ])
        file_.write(output)

    with open('products.xml', 'w') as file_:
        products = '\n\n'.join(products)
        output = '\n'.join([
            head,
            products,
            foot
            ])
        file_.write(output)

    with open('medicaments.xml', 'w') as file_:
        medicines = '\n\n'.join(medicines)
        output = '\n'.join([
            head,
            medicines,
            foot
            ])
        file_.write(output)


if __name__ == '__main__':
   main(sys.argv)
