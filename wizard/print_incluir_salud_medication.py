from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateView, StateAction
from datetime import date

class PrintIncluirSaludMedicationStart(ModelView):
    "Print Incluir Salud Medication - Start"
    __name__ = 'gnuhealth.incluir_salud.print_medication.start'


    states = fields.MultiSelection([
        ('afiliation', "Afiliation"),
        ('registered', "Registered"),
        ('renewal', "Renewal"),
        ], "Afiliation state", sort=False, required=True)

    month = fields.Selection([
        ('01', 'January'),
        ('02', 'Febreury'),
        ('03', 'March'),
        ('04', 'April'),
        ('05', 'May'),
        ('06', 'June'),
        ('07', 'July'),
        ('08', 'August'),
        ('09', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
        ], 'Month', sort=False, required=True)
    year = fields.Integer('Year', required=True)

    @staticmethod
    def default_states():
        return ['afiliation', 'registered', 'renewal']

    @staticmethod
    def default_month():
        month = date.today().month
        if month < 10:
            return f"0{month}"
        return str(month)

    @staticmethod
    def default_year():
        return date.today().year


class PrintIncluirSaludMedicationWizard(Wizard):
    "Print Incluir Salud Medication - Wizard"
    __name__ = 'gnuhealth.incluir_salud.print_medication.wizard'

    start = StateView('gnuhealth.incluir_salud.print_medication.start',
            'health_incluir_salud_fiuner.print_medication_start_view',[
                    Button("Cancel", 'end', 'tryton-cancel'),
                    Button("Print", 'print_', 'tryton-ok', default=True)
                    ])

    print_ = StateAction('health_incluir_salud_fiuner.report_incluir_salud_medication')

    def do_print_(self, action):
        data = {
                'month': self.start.month,
                'year': self.start.year,
                'states': self.start.states
                }
        return action, data
