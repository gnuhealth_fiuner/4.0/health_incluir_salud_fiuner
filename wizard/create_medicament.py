from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateView, StateTransition, StateAction
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool
from trytond.modules.health.health import Medicament


class CreateMedicamentStart(ModelView):
    "Create Medicament - Start"
    __name__ = "gnuhealth.create_medicament.start"
    name = fields.Char('Name', required=True, help='Product Name')
    active_component = fields.Char(
        'Active component', translate=True,
        help='Active Component')

    category = fields.Many2One(
        'gnuhealth.medicament.category', 'Category', select=True)

    therapeutic_action = fields.Char(
        'Therapeutic effect', help='Therapeutic action')

    composition = fields.Text('Composition', help='Components')
    indications = fields.Text('Indication', help='Indications')
    strength = fields.Float(
        'Strength',
        help='Amount of medication (eg, 250 mg) per dose')

    unit = fields.Many2One(
        'gnuhealth.dose.unit', 'dose unit',
        help='Unit of measure for the medication to be taken')

    route = fields.Many2One(
        'gnuhealth.drug.route', 'Administration Route',
        help='Drug administration route code.')

    form = fields.Many2One(
        'gnuhealth.drug.form', 'Form',
        help='Drug form, such as tablet, suspension, liquid ..')

    sol_conc = fields.Float(
        'Concentration',
        help='Solution drug concentration')

    sol_conc_unit = fields.Many2One(
        'gnuhealth.dose.unit', 'Unit',
        help='Unit of the drug concentration')

    sol_vol = fields.Float(
        'Volume',
        help='Solution concentration volume')

    sol_vol_unit = fields.Many2One(
        'gnuhealth.dose.unit', 'Unit',
        help='Unit of the solution volume')

    dosage = fields.Text('Dosage Instructions', help='Dosage / Indications')
    overdosage = fields.Text('Overdosage', help='Overdosage')
    pregnancy_warning = fields.Boolean(
        'Pregnancy Warning',
        help='The drug represents risk to pregnancy or lactancy')

    pregnancy = fields.Text(
        'Pregnancy and Lactancy', help='Warnings for Pregnant Women')

    pregnancy_category = fields.Selection([
        (None, ''),
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('X', 'X'),
        ('N', 'N'),
        ], 'Pregnancy Category',
        help='** FDA Pregnancy Categories ***\n'
        'CATEGORY A :Adequate and well-controlled human studies have failed'
        ' to demonstrate a risk to the fetus in the first trimester of'
        ' pregnancy (and there is no evidence of risk in later'
        ' trimesters).\n\n'
        'CATEGORY B : Animal reproduction studies have failed todemonstrate a'
        ' risk to the fetus and there are no adequate and well-controlled'
        ' studies in pregnant women OR Animal studies have shown an adverse'
        ' effect, but adequate and well-controlled studies in pregnant women'
        ' have failed to demonstrate a risk to the fetus in any'
        ' trimester.\n\n'
        'CATEGORY C : Animal reproduction studies have shown an adverse'
        ' effect on the fetus and there are no adequate and well-controlled'
        ' studies in humans, but potential benefits may warrant use of the'
        ' drug in pregnant women despite potential risks. \n\n '
        'CATEGORY D : There is positive evidence of human fetal  risk based'
        ' on adverse reaction data from investigational or marketing'
        ' experience or studies in humans, but potential benefits may warrant'
        ' use of the drug in pregnant women despite potential risks.\n\n'
        'CATEGORY X : Studies in animals or humans have demonstrated fetal'
        ' abnormalities and/or there is positive evidence of human fetal risk'
        ' based on adverse reaction data from investigational or marketing'
        ' experience, and the risks involved in use of the drug in pregnant'
        ' women clearly outweigh potential benefits.\n\n'
        'CATEGORY N : Not yet classified')

    presentation = fields.Text('Presentation', help='Packaging')
    adverse_reaction = fields.Text('Adverse Reactions')
    storage = fields.Text('Storage Conditions')
    is_vaccine = fields.Boolean('Vaccine')
    notes = fields.Text('Extra Info')

    active = fields.Boolean('Active', select=True)
    snomed_id = fields.Char("Snomed id")
    snomed_term = fields.Many2One('gnuhealth.medicament.snomed',
        "Snomed term",
        states={'invisible': ~Eval('search_snomed_term')}
        )
    snomed_term_fuzzy = fields.Function(
        fields.Many2One('gnuhealth.medicament.snomed',
            "Snomed term fuzzy",
            states={'invisible': Bool(Eval('search_snomed_term'))}
            ),
        'on_change_with_snomed_term_fuzzy')
    search_snomed_term = fields.Boolean('Search snomed term')
    dose_presentation = fields.Function(
        fields.Char("Dose presentation"),
        "on_change_with_dose_presentation")
    pharmacovigilance_warning = fields.Boolean('Pharmacovigilance warning')
    incluir_salud = fields.Boolean("Incluir salud")

    @staticmethod
    def default_incluir_salud():
        return True

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_therapeutic_action():
        return 'accion terapeutica'


class CreateMedicamentWizard(Wizard):
    "Create Medicament - Wizard"
    __name__ = "gnuhealth.create_medicament.wizard"

    start = StateView('gnuhealth.create_medicament.start',
            'health_incluir_salud_fiuner.gnuhealth_create_medicament_start_view',[
                          Button('Create', 'create_', 'tryton-ok', default=True),
                          Button('Cancel', 'end', 'tryton-cancel')
                          ])
    create_ = StateAction('health.gnuhealth_action_view_medicament')

    def do_create_(self, action):
        pool = Pool()
        Template = pool.get('product.template')
        Product = pool.get('product.product')
        UOM = pool.get('product.uom')
        Medicament = pool.get('gnuhealth.medicament')

        try:
            uom, = UOM.search([('name', '=', 'Unit')])
        except:
            uom, = UOM.search([('name', '=', 'Unidad')])

        # create template
        template = Template()
        template.name = self.start.name
        template.default_uom = uom.id
        template.list_price = 0
        template.save()

        product = Product()
        product.template = template.id
        product.is_medicament = True
        product.save()
        print(self.start)
        medicament = Medicament()
        medicament.name = product.id
        medicament.active_component = self.start.active_component
        medicament.category = getattr(self.start.category, 'id', None)
        # TODO el siguiente campo no existe en el formulario original
        #medicament.therapeutic_action = self.start.therapeutic_action
        medicament.composition = self.start.composition
        medicament.indications = self.start.indications
        medicament.strength = self.start.strength
        medicament.unit = getattr(self.start.unit, 'id', None)
        medicament.route = getattr(self.start.route, 'id', None)
        medicament.form = getattr(self.start.form, 'id', None)
        medicament.sol_conc = self.start.sol_conc
        medicament.sol_conc_unit = getattr(self.start.sol_conc_unit,
                                           'id', None)
        medicament.sol_vol = self.start.sol_vol
        medicament.sol_vol_unit = getattr(self.start.sol_vol_unit,
                                          'id', None)
        medicament.dosage = self.start.dosage
        medicament.overdosage = self.start.overdosage
        medicament.pregnancy_warning = self.start.pregnancy_warning
        medicament.pregnancy = self.start.pregnancy
        medicament.pregnancy_category = self.start.pregnancy_category
        medicament.presentation = self.start.presentation
        medicament.adverse_reaction = self.start.adverse_reaction
        medicament.storage = self.start.storage
        medicament.is_vaccine = self.start.is_vaccine
        medicament.notes = self.start.notes
        medicament.active = self.start.active
        medicament.snomed_id = self.start.snomed_id
        medicament.incluir_salud = self.start.incluir_salud
        # if the pharmacovigilance_warning is set on the form
        if getattr(self.start, 'pharmacovigilance_warning', None):
            medicament.pharmacovigilance_warning = self.start.pharmacovigilance_warning
        medicament.save()

        data = {'res_id': [medicament.id]}
        action['views'].reverse()
        return action, data
