from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateView, StateAction
from trytond.pool import Pool
from trytond.pyson import Eval, Bool
from trytond.i18n import gettext

from datetime import date


class CreateAfiliationStart(ModelView):
    'Create Afiliation - Start'
    __name__ = 'gnuhealth.incluir_salud.create_afiliation.start'

    message = fields.Text('Message', readonly=True)
    month = fields.Selection([
        ('01', 'January'),
        ('02', 'Febreury'),
        ('03', 'March'),
        ('04', 'April'),
        ('05', 'May'),
        ('06', 'June'),
        ('07', 'July'),
        ('08', 'August'),
        ('09', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
        ], "Month", sort=False, required=True)
    year = fields.Integer("Year", required=True)
    month_renovation = fields.Selection([
        (None, ''),
        ('01', 'January'),
        ('02', 'Febreury'),
        ('03', 'March'),
        ('04', 'April'),
        ('05', 'May'),
        ('06', 'June'),
        ('07', 'July'),
        ('08', 'August'),
        ('09', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
        ], "Month", sort=False)
    year_renovation = fields.Integer("Year",
        states={'required': Bool(Eval('month_renovation'))}
        )

    @staticmethod
    def default_message():
        return gettext('health_incluir_salud_fiuner.msg_create_afiliations')

    @staticmethod
    def default_month():
        month = date.today().month
        if month < 10:
            return f"0{month}"
        return str(month)

    @staticmethod
    def default_year():
        return date.today().year

    @staticmethod
    def default_month_renovation():
        return None


class CreateAfiliationWizard(Wizard):
    'Create Afiliation - Wizard'
    __name__ = 'gnuhealth.incluir_salud.create_afiliation.wizard'

    start = StateView('gnuhealth.incluir_salud.create_afiliation.start',
            'health_incluir_salud_fiuner.create_afiliations_start_view',[
                    Button("Cancel", 'end', 'tryton-cancel'),
                    Button("Ok", 'create_', 'tryton-ok', default=True)
                    ])

    create_ = StateAction('health_incluir_salud_fiuner.act_gnuhealth_incluir_salud_afiliation')

    def do_create_(self, action):
        pool = Pool()
        Afiliation = pool.get('gnuhealth.incluir_salud.afiliation')
        Patient = pool.get('gnuhealth.patient')

        patients = Patient.search([('incluir_salud', '=', True)])
        data = []
        # Afiliamos los pacientes que tengan incluir salud
        for patient in patients:
            afiliations = Afiliation.search([
                                ('patient', '=', patient.id),
                                ])
            print('afiliaciones')
            if not afiliations:
                afiliation = Afiliation()
                afiliation.state_afiliation = 'afiliation'
                afiliation.month = self.start.month
                afiliation.year = self.start.year
                afiliation.patient = patient.id
                afiliation.save()
                data.append(afiliation)

        # Actualizamos el mes y año de los registros de afiliación
        print('actualizaciones')
        afiliations_update = Afiliation.search([
            ('patient', 'not in', [d.patient.id for d in data]),
            ('month', '=', self.start.month_renovation),
            ('year', '=', self.start.year_renovation),
            ])
        for afiliation in afiliations_update:
            state_afiliation = afiliation.state_afiliation
            print(afiliation.patient.rec_name)
            afiliation.year = self.start.year
            afiliation.month = self.start.month
            afiliation.state_afiliation = 'renewal' \
                if state_afiliation in ['registered', 'renewal'] \
                else state_afiliation
            afiliation.save()
            data.append(afiliation)

        data = {'res_id': [d.id for d in data]}
        return action, data
