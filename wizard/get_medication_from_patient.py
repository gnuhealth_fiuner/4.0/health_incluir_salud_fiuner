from trytond.wizard import Wizard, StateAction
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, PYSONEncoder


class GetMedicationFromPatient(Wizard):
    'Get Medication From Patient'
    __name__ = 'gnuhealth.incluir_salud.get_medication'

    start = StateAction('health_incluir_salud_fiuner.act_patient_medication')

    def do_start(self, actions):
        pool = Pool()
        Afiliation = pool.get('gnuhealth.incluir_salud.afiliation')
        Medication = pool.get('gnuhealth.patient.medication')

        afiliation = Afiliation(Transaction().context.get('active_id'))
        patient = afiliation.patient
        medications = Medication.search([('name', '=', afiliation.patient.id)])
        data = {'res_id': [m.id for m in medications if m]}

        return actions, data
