from trytond.wizard import Wizard, Button, StateView, StateAction

from .print_incluir_salud_medication import PrintIncluirSaludMedicationStart

from datetime import date

class PrintAfiliationStateStart(PrintIncluirSaludMedicationStart):
    "Print Afiliation State - Start"
    __name__ = 'gnuhealth.incluir_salud.print_afiliation_state.start'


class PrintAfiliationStateWizard(Wizard):
    "Print Afiliation Start - Wizard"
    __name__ = 'gnuhealth.incluir_salud.print_afiliation_state.wizard'

    start = StateView('gnuhealth.incluir_salud.print_afiliation_state.start',
            'health_incluir_salud_fiuner.print_medication_start_view',[
                    Button("Cancel", 'end', 'tryton-cancel'),
                    Button("Print", 'print_', 'tryton-ok', default=True)
                    ])

    print_ = StateAction('health_incluir_salud_fiuner.report_afiliation_state')

    def do_print_(self, action):
        data = {
                'month': self.start.month,
                'year': self.start.year,
                'states': self.start.states
                }
        return action, data
