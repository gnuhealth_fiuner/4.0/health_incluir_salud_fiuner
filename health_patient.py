from trytond.model import fields
from trytond.pool import PoolMeta


class PatientData(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient'

    incluir_salud = fields.Boolean("Incluir salud", help="Incluir en tramites de incluir salud")
    afiliations = fields.One2Many('gnuhealth.incluir_salud.afiliation', 'patient', "Afiliations")

    @staticmethod
    def default_incluir_salud():
        return False
