from trytond.pool import Pool

from . import configuration
from . import health_incluir_salud
from . import health_patient
from . import health_medicament

from .wizard import create_afiliations
from .wizard import get_medication_from_patient
from .wizard import print_incluir_salud_medication
from .wizard import print_afiliation_state
from .wizard import create_medicament

from .report import print_incluir_salud_medication_report
from .report import print_afiliation_state_report


def register():
    Pool.register(
        configuration.IncluirSaludConfig,
        configuration.IncluirSaludConfigCheck,
        health_incluir_salud.Afiliation,
        health_incluir_salud.AfiliationMedication,
        health_incluir_salud.AfiliationCheck,
        health_patient.PatientData,
        health_medicament.Medicament,
        create_afiliations.CreateAfiliationStart,
        print_incluir_salud_medication.PrintIncluirSaludMedicationStart,
        print_afiliation_state.PrintAfiliationStateStart,
        create_medicament.CreateMedicamentStart,
        module='health_incluir_salud_fiuner', type_='model')
    Pool.register(
        create_afiliations.CreateAfiliationWizard,
        get_medication_from_patient.GetMedicationFromPatient,
        print_incluir_salud_medication.PrintIncluirSaludMedicationWizard,
        print_afiliation_state.PrintAfiliationStateWizard,
        create_medicament.CreateMedicamentWizard,
        module='health_incluir_salud_fiuner', type_='wizard')
    Pool.register(
        print_incluir_salud_medication_report.PrintIncluirSaludMedicationReport,
        print_afiliation_state_report.PrintAfiliationState,
        module='health_incluir_salud_fiuner', type_='report')
