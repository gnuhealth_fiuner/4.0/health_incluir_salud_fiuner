from trytond.model import fields
from trytond.pool import PoolMeta


class Medicament(metaclass=PoolMeta):
    __name__ = 'gnuhealth.medicament'

    incluir_salud = fields.Boolean("Incluir salud")

