from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.pyson import Eval, Bool
from trytond.i18n import gettext

from .exceptions import UniquePatientPeriod

from datetime import date


class Afiliation(ModelSQL, ModelView):
    "Check Afiliation"
    __name__ = 'gnuhealth.incluir_salud.afiliation'

    patient = fields.Many2One('gnuhealth.patient', 'Patient', required=True,
                    domain=[('incluir_salud', '=', True)])
    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date')
    month = fields.Selection([
        ('01', 'January'),
        ('02', 'Febreury'),
        ('03', 'March'),
        ('04', 'April'),
        ('05', 'May'),
        ('06', 'June'),
        ('07', 'July'),
        ('08', 'August'),
        ('09', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
        ], 'Month', sort=False, required=True)
    month_string = month.translated('month')
    year = fields.Integer('Year', required=True)
    observations = fields.Text('Observations')
    healthprof = fields.Many2One('gnuhealth.healthprofessional',
            "Health Professional")
    medications = fields.One2Many('gnuhealth.incluir_salud.medication',
            'afiliation', "Medications")
    state_afiliation = fields.Selection([
        (None, ''),
        ('registered', "Registered"),
        ('renewal', "Renewal"),
        ('afiliation', "Afiliation")
        ], "Afiliation state", sort=False)
    state_afiliation_string = state_afiliation.translated('state_afiliation')
    documentation_state = fields.Selection([
        (None, ''),
        ('patient_dont_present_docs', 'Patient don\'t present docs '),
        ('hp_dont_present_docs', 'Health prof don\'t present docs')
        ], 'Documentation state', sort=False,
        states={ 'readonly': Bool(Eval('state_afiliation'))}
        )
    documentation_state_string = \
            documentation_state.translated('documentation_state')

    def get_rec_name(self, name):
        if self.month and self.year:
            return f"{self.month}/{self.year}  -  {self.patient}"
        else:
            return self.patient.rec_name
        return ''

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('patient',) + tuple(clause[1:]),
            ]

    @fields.depends('state_afiliation')
    def on_change_with_documentation_state(self, name=None):
        if self.state_afiliation:
            return ''
        return

    @staticmethod
    def default_state_afiliation():
        return 'afiliation'

    @staticmethod
    def default_start_date():
        return date.today()

    @staticmethod
    def default_month():
        month = date.today().month
        if month < 10:
            return f"0{month}"
        return str(month)

    @staticmethod
    def default_year():
        return date.today().year

    @staticmethod
    def default_check_list():
        pool = Pool()
        Config = pool.get('gnuhealth.incluir_salud.config')
        config = Config(1)
        res = [{
            'name': c.name,
            'check': False
               } for c in config.check_list]
        return res

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('patient_unique', Unique(t, t.patient),
            'This patient is already registered')
        ]
        cls._order.insert(0, ('patient', 'ASC'))


class AfiliationMedication(ModelSQL, ModelView):
    "Afiliation - Medication"
    __name__ = 'gnuhealth.incluir_salud.medication'

    afiliation = fields.Many2One('gnuhealth.incluir_salud.afiliation',
            "Afiliation", required=True)
    medication = fields.Many2One('gnuhealth.medicament', "Medication",
            required=True)
    qty = fields.Float("Quantity", required=True)
    monthly_unique = fields.Boolean('Unique monthly vial')
    monthly = fields.Function(
            fields.Integer("Monthly quantity"),
            'on_change_with_monthly')

    @fields.depends('qty', 'monthly_unique')
    def on_change_with_monthly(self, name=None):
        if self.qty and not self.monthly_unique:
            return self.qty * 30
        elif self.monthly_unique:
            return self.qty
        return 0

    @staticmethod
    def default_monthly_unique():
        return False


class AfiliationCheck(ModelSQL, ModelView):
    "Incluir Salud - Check"
    __name__ = 'gnuhealth.incluir_salud.check_list'

    afiliation = fields.Many2One('gnuhealth.incluir_salud.afiliation', "Afiliation")
    name = fields.Char("Name", required=True)
    check = fields.Boolean("Check")
    observation = fields.Char("Observation")
