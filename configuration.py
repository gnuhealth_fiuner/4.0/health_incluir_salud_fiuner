from trytond.model import ModelSQL, ModelView, ModelSingleton, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)


class IncluirSaludConfig(ModelSingleton, ModelSQL, ModelView,
                CompanyMultiValueMixin):
    "Incluir Salud Configuration"
    __name__ = 'gnuhealth.incluir_salud.config'

    check_list = fields.One2Many(
        'gnuhealth.incluir_salud.config.check', 'config', "Check list")


class IncluirSaludConfigCheck(ModelSQL, ModelView, CompanyValueMixin):
    "Incluir Salud Configuration - Check"
    __name__ = 'gnuhealth.incluir_salud.config.check'

    config = fields.Many2One('gnuhealth.incluir_salud.config',
            "Configuration", ondelete='CASCADE',
            context={ 'company': Eval('company', -1),})
    name = fields.Char("Name", required=True)
