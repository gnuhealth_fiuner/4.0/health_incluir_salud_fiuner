from trytond.exceptions import UserError


class UniquePatientPeriod(UserError):
    pass
